# OroDev (Vagrantfile)

OroPlatform (and OroCRM, OroCommerce) Vagrantfile. Fit to your needs.


SET UP THE BOX BASICS:

  - config.vm.box (np. "bento/ubuntu-16.04")
  - Sconfig.vm.provider (np. "virtualbox")
  - v.name ("orodev_localhost")
  - v.memory (np. 1024)
  - v.cpus (np. 1)

SET UP THE FOLDERS:

```sh
config.vm.synced_folder ".", "/var/www/", :mount_options => ["dmode=777", "fmode=777"]
```
or nfs

```sh
config.vm.synced_folder ".", "/var/www/", :nfs => { :mount_options => ["dmode=777","fmode=777"] } 
```

ADD PORT FORWARDS AND PRIVATE NETWORK:
```sh
config.vm.network "forwarded_port", guest: 80, host: 8000        
config.vm.network "private_network", ip: "192.168.33.10"
```

That's all!